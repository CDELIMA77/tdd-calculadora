package com.calculadora.calculadora;

import java.util.List;

public class Operacao {

    public static int soma(int num1, int num2) {
        int resultado = num1 + num2 ;
        return resultado;
    }

    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero:numeros){
            resultado += numero;
        }
        return resultado;
    }


    public static int subtrai(int num1, int num2) {
        int resultado = num1 - num2 ;
        return resultado;
    }

    public static int subtrai(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero:numeros){
              if (numeros.indexOf(numero) == 0 ) {
                resultado = numero;
            } else {
                resultado -= numero;
            }
        }
        return resultado;
    }


    public static int multiplicacao(int num1, int num2) {
        int resultado = num1 * num2 ;
        return resultado;
    }

    public static int multiplicacao(List<Integer> numeros){
        int resultado = 1;
        for (int numero : numeros) {
            resultado *= numero;
        }
        return resultado;
    }

    public static double divisao(int num1, int num2){
        double resultado = 0;

        if ((num1 == 0 ) || (num2 == 0)) {
            return resultado ;
        } else {
           if (num1 >= num2) {
              resultado = num1 / num2;
           } else {
              resultado = num2 / num1;
                  }
           }

        return resultado;
     }
}
