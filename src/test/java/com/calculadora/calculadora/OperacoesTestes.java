package com.calculadora.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTestes {

    /*@Test Vai gerar o teste sem precisar do main */
    @Test
    public void testarOperacaoDeSoma() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);

        Assertions.assertEquals(Operacao.soma(numeros), 6);
    }

    @Test
    public void testarOperacaoDeSomaDoisNumeros(){
        Assertions.assertEquals(Operacao.soma(1,2), 3);
    }

    @Test
    public void testarOperacaoDeSubtracao() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(5);
        numeros.add(2);
        numeros.add(1);

        Assertions.assertEquals(Operacao.subtrai(numeros), 2);
    }

    @Test
    public void testarOperacaoSubtracaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.subtrai(3,2), 1);
    }


    @Test
    public void testarOperacaoDeMultiplicacao() {
        List<Integer> numeros = new ArrayList<>();
        numeros.add(5);
        numeros.add(2);
        numeros.add(1);

        Assertions.assertEquals(Operacao.multiplicacao(numeros), 10);
    }

    @Test
    public void testarOperacaoMultiplicacaoDeDoisNumeros(){
        Assertions.assertEquals(Operacao.multiplicacao(3,2), 6);
    }

    @Test
    public void testarOperacaoDivisaoDeDoisNumeros1(){
        Assertions.assertEquals(Operacao.divisao(2,6), 3);
    }

    @Test
    public void testarOperacaoDivisaoDeDoisNumeros2(){
        Assertions.assertEquals(Operacao.divisao(6,2), 3);
    }

    @Test
    public void testarValorInvalidoNaDivisao(){
        Assertions.assertEquals(Operacao.divisao(0, 2), 0);
        /*Assertions.assertEquals(Operacao.divisao(0, 2), "Valor 0 e invalido");
        Assertions.assertThrows(ArithmeticException.class, () -> {Operacao.divisao(0,2);});*/
    }


}